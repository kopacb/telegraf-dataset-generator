import time
import math
import random
import http.server
import socketserver
import json


k = 5
PORT = 8000

start_time = time.time()
# Handler = http.server.SimpleHTTPRequestHandler


# HTTPRequestHandler class
class JSONHandler(http.server.SimpleHTTPRequestHandler):
    # GET
    def do_GET(self):
        global k
        if random.randint(0,100) > 95:
            k = random.randint(0, 10)
        dataset = {
            'sin': 7*math.sin((time.time() - start_time)/60),
            'cos': k * math.cos((time.time() - start_time)/20),
            'k': k,
            'jitter': 20 + 2 * random.random(),
            'jitter2': 20 + 5 * random.random(),
            'rnd': 20 * random.random(),
            'rnd2': start_time + time.time() * random.random()
        }
        j_dataset = json.dumps(dataset)
        # Send response status code
        self.send_response(200)

        # Send headers
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        # Send message back to client
        message = j_dataset
        # Write content as utf-8 data
        self.wfile.write(bytes(message, "utf8"))
        return


Handler = JSONHandler


httpd = socketserver.TCPServer(("", PORT), Handler)
print("serving at port", PORT)
httpd.serve_forever()